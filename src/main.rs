// SPDX-License-Identifier: MIT OR Apache-2.0
//
// Copyright (c) 2018-2022 Andre Richter <andre.o.richter@gmail.com>

#![feature(asm_const)]
#![no_main]
#![no_std]

use core::arch::asm;

use tock_registers::{
    interfaces::Writeable, register_bitfields, register_structs, registers::ReadWrite,
};

mod panic_wait;

register_bitfields! [
    u32,

    GPFSEL2 [
        PIN25 OFFSET(15) NUMBITS(3) [
            Output = 0b001,
        ]
    ],

    GPSET0 [
        PIN25 OFFSET(25) NUMBITS(1) [
            Set = 0b1,
        ]
    ],

    GPCLR0 [
        PIN25 OFFSET(25) NUMBITS(1) [
            Clr = 0b1,
        ]
    ],
];

register_structs! {
    #[allow(non_snake_case)]
    pub RegisterBlock {
        (0x00 => _reserved0),
        (0x08 => GPFSEL2: ReadWrite<u32, GPFSEL2::Register>),
        (0x0c => _reserved1),
        (0x1c => GPSET0: ReadWrite<u32, GPSET0::Register>),
        (0x20 => _reserved2),
        (0x28 => GPCLR0: ReadWrite<u32, GPCLR0::Register>),
        (0x2c => @END),
    }
}

#[no_mangle]
#[link_section = ".text._start_arguments"]
pub static BOOT_CORE_ID: u64 = 0;

unsafe fn kernel_init() -> ! {
    let registers = &*(0xfe200000 as *const RegisterBlock);

    registers.GPFSEL2.write(GPFSEL2::PIN25::Output);
    registers.GPSET0.write(GPSET0::PIN25::Set);

    panic!();

    // loop {
    //     for _ in 0..50000 {
    //         asm!("nop");
    //     }

    //     registers.GPSET0.write(GPSET0::PIN25::Set);

    //     for _ in 0..50000 {
    //         asm!("nop");
    //     }

    //     registers.GPCLR0.write(GPCLR0::PIN25::Clr);
    // }
}
