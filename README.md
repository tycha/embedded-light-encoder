# Embedded Light Encoder

A bare metal operating system for the raspberry pi that drives programmable lights and music output

Based off: [https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials)
